import java.util.Scanner;

public class Shop
{
	public static void main(String[] args)
	{
		Scanner read = new Scanner(System.in);
		
		Car[] sedanCars = new Car[4];
		
		for (int i=0; i < sedanCars.length; i++)
		{
			
			System.out.println("For car " + (i+1) +",");
			
			System.out.println("Enter a year between 1991 and 2022");
			int theCarYear = read.nextInt();
			
			System.out.println("Enter a car colour");
			String theCarColour = read.next();
			
			System.out.println("Enter a car make");
			String theCarMake = read.next();
			
			System.out.println("Enter a model for that make");
			String theCarModel = read.next();
			
			sedanCars[i] = new Car(theCarYear, theCarColour, theCarMake, theCarModel);
			
			System.out.println("");
		}
		
		System.out.println("");
		System.out.println(sedanCars[3].getCarYear());
		System.out.println(sedanCars[3].getCarColour());
		System.out.println(sedanCars[3].getCarMake());
		System.out.println(sedanCars[3].getCarModel());
		System.out.println("");
		
		System.out.println("Now enter a new year between 1991 and 2022");
		int theCarYear = read.nextInt();
		sedanCars[3].setCarYear(theCarYear);
			
		System.out.println("Now enter a new car colour");
		String theCarColour = read.next();
		sedanCars[3].setCarColour(theCarColour);
			
		System.out.println("Now enter a new car make");
		String theCarMake = read.next();
		sedanCars[3].setCarMake(theCarMake);
		
		System.out.println("");
		System.out.println(sedanCars[3].getCarYear());
		System.out.println(sedanCars[3].getCarColour());
		System.out.println(sedanCars[3].getCarMake());
		System.out.println(sedanCars[3].getCarModel());
		System.out.println("");
		
		sedanCars[3].pitchACar();
		
	}
}