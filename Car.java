public class Car
{
	private int carYear;
	private String carColour;
	private String carMake;
	private String carModel;
	
	public void setCarYear(int newCarYear)
	{
		this.carYear = newCarYear;
	}
	
	public void setCarColour(String newCarColour)
	{
		this.carColour = newCarColour;
	}
	
	public void setCarMake(String newCarMake)
	{
		this.carMake = newCarMake;
	}
	
	public int getCarYear()
	{
		return this.carYear;
	}
	
	public String getCarColour()
	{
		return this.carColour;
	}
	
	public String getCarMake()
	{
		return this.carMake;
	}
	
	public String getCarModel()
	{
		return this.carModel;
	}
	
	public Car(int carYear, String carColour, String carMake, String carModel)
	{
		this.carYear = carYear;
		this.carColour = carColour;
		this.carMake = carMake;
		this.carModel = carModel;
	}
	
	public void pitchACar()
	{
		System.out.println("This is the car for you!");
		System.out.println("You're looking at a " + carYear + " " + carColour + " " + carMake + " " + carModel + ".");
		System.out.println("Order right now, and if you have a cat, we'll throw in a Catillac for free!*");
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("*Standard 50% original-price-of-Catillac tax rate applies on free Catillacs, offer not optional.......");
	}
}